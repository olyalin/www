<div class="container">
    <div class="row">
        <?php foreach($ferrariteam as $item): ?>
            <div class="col-lg-8 col-xl-8 col-md-8">
                <h1><?= $item->getTeljesnev(); ?></h1>
                <p><b>Ország: </b><?= $item->getOrszag(); ?></p>
                <p><b>Motor: </b><?= $item->getMotor(); ?></p>
                <p><b>Kezdés éve: </b><?= $item->getKezdes(); ?></p>
            </div>
            <div class="col-lg-4 col-xl-4 col-md-4">
                <img src="img/teams/<?= $item->getLogo(); ?>" alt="" class="img-fluid">
            </div>
        <?php endforeach; ?>
        <?php foreach ($ferraridrivers as $item):?>
            <div class="col-lg-6 col-xl-6 col-md-6 mt-5">
                <h2><?= $item->getNev(); ?></h2>
                <p>Rajtszam: <?= $item->getRajtszam(); ?></p>
                <p>Szuletett: <?= $item->getSzuletett(); ?></p>
                <p>Vb: <?= $item->getVb(); ?></p>
                <img src="img/drivers/<?= $item->getKep(); ?>" alt="" class="img-fluid img-thumbnail d-block mx-auto" style="width: 70%">
            </div>
        <?php endforeach; ?>
        <a href="index.php?page=home" class="btn btn-primary m-3 d-block mx-auto">Vissza...</a>
    </div>
</div>
