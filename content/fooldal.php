<div class="container">
    <div class="row">
        <?php foreach($teams as $item): ?>
            <div class="col-4" id="nev"><?= $item->getNev();?></div>
            <div class="col-4"><img src="img/teams/<?= $item->getLogo(); ?>" class="img-fluid" alt=""></div>
            <div class="col-4 m-auto mx-auto" id="gomb"><a href="index.php?page=<?= $item->getNev(); ?>" class="btn btn-primary">Info</a></div>
        <?php endforeach; ?>
    </div>
</div>