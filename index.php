<?php
use model\Csapat;
use model\Pilota;
spl_autoload_register(function($class){
   include('src/' . str_replace('\\', '/', $class) . '.php');
});

$teams = Csapat::findExistingTeams();
$mercedesdrivers = Pilota::getMercedesDrivers();
$ferraridrivers = Pilota::getFerrariDrivers();
$mercedesteam = Csapat::getTeam('mercedes');
$ferrariteam = Csapat::getTeam('ferrari');

$content = "fooldal.php";
if(isset($_GET['page'])){
    switch ($_GET['page']){
        case "Mercedes":
            $content = 'mercedes.php';
            break;
        case "Ferrari":
            $content = 'ferrari.php';
            break;
        case "home":
            $content='fooldal.php';
            break;
    }
}
?>

<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Forma 1</title>
</head>
<body>
    <?php include("content/$content")?>
</body>
</html>