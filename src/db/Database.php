<?php
namespace db;
class Database
{
    /** @var  */
    private static $pdo;

    /**
     * @return \PDO
     */
    public static function connect(){
        $conf = include('config.php');
        $db = $conf['db'];
        if(self::$pdo == null){
            self::$pdo = new \PDO("mysql:host=" . $db["db_host"] . ";dbname=" . $db["db_name"] . ";charset=" . $db["db_encoding"], $db["db_user"], $db["db_password"]);
        }
        return self::$pdo;
    }
}