<?php
namespace model;
use db\Database;
class Pilota
{
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCsapatId()
    {
        return $this->csapat_id;
    }

    /**
     * @return mixed
     */
    public function getRajtszam()
    {
        return $this->rajtszam;
    }

    /**
     * @return mixed
     */
    public function getNev()
    {
        return $this->nev;
    }

    /**
     * @return mixed
     */
    public function getSzuletett()
    {
        return $this->szuletett;
    }

    /**
     * @return mixed
     */
    public function getVb()
    {
        return $this->vb;
    }

    /**
     * @return mixed
     */
    public function getKep()
    {
        return $this->kep;
    }

    /**
     * @return mixed
     */
    public function getLeiras()
    {
        return $this->leiras;
    }
    private $id;
    private $csapat_id;
    private $rajtszam;
    private $nev;
    private $szuletett;
    private $vb;
    private $kep;
    private $leiras;

    private static $conn;

    public static function getMercedesDrivers(){
        self::$conn = Database::connect();
        $statement = self::$conn->prepare('SELECT * FROM f1.pilota WHERE csapat_id IN(SELECT id FROM f1.csapat WHERE nev = :nev)');
        $statement->execute([':nev' => 'Mercedes']);
        return $statement->fetchAll(\PDO::FETCH_CLASS, self::class);
    }

    public static function getFerrariDrivers(){
        self::$conn = Database::connect();
        $statement = self::$conn->prepare('SELECT * FROM f1.pilota WHERE csapat_id IN(SELECT id FROM f1.csapat WHERE nev = :nev)');
        $statement->execute([':nev' => 'Ferrari']);
        return $statement->fetchAll(\PDO::FETCH_CLASS, self::class);
    }

}