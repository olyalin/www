<?php
namespace model;
use db\Database;
class Csapat
{
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNev()
    {
        return $this->nev;
    }

    /**
     * @return mixed
     */
    public function getTeljesnev()
    {
        return $this->teljesnev;
    }

    /**
     * @return mixed
     */
    public function getOrszag()
    {
        return $this->orszag;
    }

    /**
     * @return mixed
     */
    public function getMotor()
    {
        return $this->motor;
    }

    /**
     * @return mixed
     */
    public function getKezdes()
    {
        return $this->kezdes;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }
    private $id;
    private $nev;
    private $teljesnev;
    private $orszag;
    private $motor;
    private $kezdes;
    private $logo;

    private static $conn;

    public static function findExistingTeams(){
        self::$conn = Database::connect();
        $statement = self::$conn->prepare('SELECT * FROM f1.csapat WHERE nev = :nev1 OR nev = :nev2');
        $statement->execute([':nev1' => 'Ferrari', ':nev2' => 'Mercedes']);
        return $statement->fetchAll(\PDO::FETCH_CLASS, self::class);
    }

    public static function getTeam($t){
        ucfirst($t);
        self::$conn = Database::connect();
        $statement = self::$conn->prepare('SELECT * FROM f1.csapat WHERE nev = :nev');
        $statement->execute([':nev' => $t]);
        return $statement->fetchAll(\PDO::FETCH_CLASS, self::class);
    }
}